package net.chenlin.dp.ids.common.entity;

import java.io.Serializable;

/**
 * ticket校验DTO
 * @author zhouchenglin[yczclcn@163.com]
 */
public class TicketValidateResultDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * sessionId
     */
    private String sessionId;

    /**
     * 是否记住我
     */
    private Boolean rememberMe;

    /**
     * 登录类型：1：web端，2：app端
     */
    private Integer loginType;

    /**
     * constructor
     */
    public TicketValidateResultDTO() { }

    /**
     * constructor
     * @param sessionId
     * @param rememberMe
     * @param loginType
     */
    public TicketValidateResultDTO(String sessionId, Boolean rememberMe, Integer loginType) {
        this.sessionId = sessionId;
        this.rememberMe = rememberMe;
        this.loginType = loginType;
    }

    /**
     * getter for sessionId
     * @return
     */
    public String getSessionId() {
        return sessionId;
    }

    /**
     * setter for sessionId
     * @param sessionId
     */
    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

    /**
     * getter for rememberMe
     * @return
     */
    public Boolean getRememberMe() {
        return rememberMe;
    }

    /**
     * setter for rememberMe
     * @param rememberMe
     */
    public void setRememberMe(Boolean rememberMe) {
        this.rememberMe = rememberMe;
    }

    /**
     * getter for loginType
     * @return
     */
    public Integer getLoginType() {
        return loginType;
    }

    /**
     * setter for loginType
     * @param loginType
     */
    public void setLoginType(Integer loginType) {
        this.loginType = loginType;
    }

    /**
     * to string
     * @return
     */
    @Override
    public String toString() {
        return "TicketValidateResultDTO{" +
                "sessionId='" + sessionId + '\'' +
                ", rememberMe=" + rememberMe +
                ", loginType=" + loginType +
                '}';
    }

}
